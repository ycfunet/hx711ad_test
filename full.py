import sys

from hx711ad_reader import Hx711adReader

if __name__ == '__main__':

    w = sys.argv[1]

    hx711 = Hx711adReader()
    hx711.Open("/dev/ttyUSB0")

    weight = hx711.CalibrateFull(int(w))
    print(weight)

    hx711.Close()
    sys.exit(0)
