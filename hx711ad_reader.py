import serial
import binascii
import time

CMD_DELAY = 0.3

class Hx711adReader:

    MID = 0x00
    DELAY_TIME = 1.0

    SERIAL_PORT = None
    IS_OPEN = False
    NEED_REOPEN = False

    SERIAL_PORT_NAME = None
    SERIAL_PORT_BAUDRATE = None

    @staticmethod
    def StringToHexByte(s):
        x = int(s, 10)
        y = chr(x)
        return y

    @staticmethod
    def HexStringToInt(s):
        return int(s, 16)

    @staticmethod
    def IntToHexString(i):
        return format(i, '02x')

    @staticmethod
    def ByteHexToHexString(s):
        return binascii.hexlify(s)

    # A3 MID A2 A4 XORSUM
    def GetWeight(self):
        if self.NEED_REOPEN:
            self.NEED_REOPEN = False
            ret = self.Reopen()
            if ret is False:
                return ""

        if self.IS_OPEN is False:
            return ""

        if not self.SERIAL_PORT.isOpen():
            ret = self.Reopen()
            if ret is False:
                return ""

        xorsum = int("a3", 16) ^ self.MID ^ int("a2", 16) ^ int("a4", 16)
        cmd_int = [ int("a3", 16), self.MID, int("a2", 16), int("a4", 16), xorsum ]
        cmd = bytearray(cmd_int)

        self.SERIAL_PORT.write(cmd)
        time.sleep(CMD_DELAY)
        result = self.SERIAL_PORT.read_all()

        if len(result) != 10:
            if len(result) <= 0:
                self.NEED_REOPEN = True

            print("Len = " + str(len(result)))
            return ""

        if result[0] != 0xaa:
            print("Return CMD Error => " + result[0])
            return ""

        if result[9] != 0xff:
            print("Return CMD Error => " + result[9])
            return ""

        if result[1] != 0xa3:
            print("Return CMD Error => " + result[0])
            return ""

        sum1 = result[1] + result[2] + result[3] + result[4] + result[5] + result[6]
        sum2 = (result[7] << 8) + result[8]
        if sum1 != sum2:
            print("Return Checksum Error => " + sum1 + " " + sum2)
            return ""

        posneg = 1
        if result[3] == 0x01:
            posneg = -1
        d = posneg * ((result[4] << 16) | (result[5] << 8) | result[6])

        return d

    # A1 MID A0 A2 XORSUM
    def GetRaw(self):
        if self.NEED_REOPEN:
            self.NEED_REOPEN = False
            ret = self.Reopen()
            if ret is False:
                return ""

        if self.IS_OPEN is False:
            return ""

        if not self.SERIAL_PORT.isOpen():
            ret = self.Reopen()
            if ret is False:
                return ""

        xorsum = int("a1", 16) ^ self.MID ^ int("a0", 16) ^ int("a2", 16)
        cmd_int = [ int("a1", 16), self.MID, int("a0", 16), int("a2", 16), xorsum ]
        cmd = bytearray(cmd_int)

        self.SERIAL_PORT.write(cmd)
        time.sleep(CMD_DELAY)
        result = self.SERIAL_PORT.read_all()

        if len(result) != 10:
            if len(result) <= 0:
                self.NEED_REOPEN = True

            print("Len = " + str(len(result)))
            return ""

        if result[0] != 0xaa:
            print("Return CMD Error => " + result[0])
            return ""

        if result[9] != 0xff:
            print("Return CMD Error => " + result[9])
            return ""

        if result[1] != 0xa1:
            print("Return CMD Error => " + result[0])
            return ""

        sum1 = result[1] + result[2] + result[3] + result[4] + result[5] + result[6]
        sum2 = (result[7] << 8) + result[8]
        if sum1 != sum2:
            print("Return Checksum Error => " + sum1 + " " + sum2)
            return ""

        d = (result[4] << 16) | (result[5] << 8) | result[6]
        dhex = self.IntToHexString(d)
        sdhex = str("{} H").format(dhex.upper())

        return sdhex

    def Reopen(self):
        print("Reopen")
        if self.IS_OPEN is True or self.SERIAL_PORT.isOpen():
            print("Close")
            self.Close()

        if self.SERIAL_PORT_NAME is not None and self.SERIAL_PORT_BAUDRATE is not None:
            return self.Open(self.SERIAL_PORT_NAME, self.SERIAL_PORT_BAUDRATE)

        return False

    def Open(self, serial_name, mid=0x00, baud_rate=9600):
        print("Open")
        if self.IS_OPEN is True and self.SERIAL_PORT.isOpen():
            self.Close()

        try:
            self.MID = mid
            self.SERIAL_PORT = serial.Serial(serial_name, baud_rate, timeout=5)
            self.IS_OPEN = True
            self.SERIAL_PORT_NAME = serial_name
            self.SERIAL_PORT_BAUDRATE = baud_rate
            return True
        except serial.SerialException:
            self.IS_OPEN = False
            return False

    def Close(self):
        print("Close")
        if self.SERIAL_PORT.isOpen():
            self.SERIAL_PORT.close()

        self.IS_OPEN = False

    # AD MID high_weight low_weight xorsum
    def CalibrateFull(self, weight):
        high_weight = weight >> 8
        low_weight = weight & 255
        print(high_weight)
        print(low_weight)

        if self.IS_OPEN is False:
            return False

        if not self.SERIAL_PORT.isOpen():
            return False

        xorsum = int("ad", 16) ^ self.MID ^ high_weight ^ low_weight
        cmd_int = [ int("ad", 16), self.MID, high_weight, low_weight, xorsum ]
        print(cmd_int)
        cmd = bytearray(cmd_int)

        self.SERIAL_PORT.write(cmd)
        time.sleep(CMD_DELAY)
        result = self.SERIAL_PORT.read_all()
        print(result)

        if len(result) <= 0:
            return False

        if len(result) != 10:
            return False

        if result[0] != 0xaa:
            return False

        if result[9] != 0xff:
            return False

        if result[2] != self.MID:
            return False

        posneg = 1
        if result[3] == 0x01:
            posneg = -1
        d = posneg * ((result[4] << 16) | (result[5] << 8) | result[6])

        return d

        return True

    # AC MID AB AD xorsum
    def RestoreZero(self):
        if self.IS_OPEN is False:
            return False

        if not self.SERIAL_PORT.isOpen():
            return False

        xorsum = int("ac", 16) ^ self.MID ^ int("ab", 16) ^ int("ad", 16)
        cmd_int = [ int("ac", 16), self.MID, int("ab", 16), int("ad", 16), xorsum ]
        cmd = bytearray(cmd_int)

        self.SERIAL_PORT.write(cmd)
        time.sleep(CMD_DELAY)
        result = self.SERIAL_PORT.read_all()

        if len(result) <= 0:
            return ""

        if len(result) != 10:
            return ""

        if result[0] != 0xaa:
            return ""

        if result[9] != 0xff:
            return ""

        if result[2] != self.MID:
            return ""

        if result[1] != 0xac:
            return ""

        sum1 = result[1] + result[2] + result[3] + result[4] + result[5] + result[6]
        sum2 = (result[7] << 8) + result[8]
        if sum1 != sum2:
            return ""

        posneg = 1
        if result[3] == 0x01:
            posneg = -1
        d = posneg * ((result[4] << 16) | (result[5] << 8) | result[6])

        return d

    # AB MID AA AC xorsum
    def ClearZero(self):
        if self.IS_OPEN is False:
            return ""

        if not self.SERIAL_PORT.isOpen():
            return ""

        xorsum = int("ab", 16) ^ self.MID ^ int("aa", 16) ^ int("ac", 16)
        cmd_int = [ int("ab", 16), self.MID, int("aa", 16), int("ac", 16), xorsum ]
        cmd = bytearray(cmd_int)

        self.SERIAL_PORT.write(cmd)
        time.sleep(CMD_DELAY)
        result = self.SERIAL_PORT.read_all()

        if len(result) <= 0:
            return ""

        if len(result) != 10:
            return ""

        if result[0] != 0xaa:
            return ""

        if result[9] != 0xff:
            return ""

        if result[2] != self.MID:
            return ""

        if result[1] != 0xab:
            return ""

        sum1 = result[1] + result[2] + result[3] + result[4] + result[5] + result[6]
        sum2 = (result[7] << 8) + result[8]
        if sum1 != sum2:
            return ""

        posneg = 1
        if result[3] == 0x01:
            posneg = -1
        d = posneg * ((result[4] << 16) | (result[5] << 8) | result[6])

        return d

        # return True

    # AA MID A9 AB xorsum
    def CalibrateZero(self):
        if self.IS_OPEN is False:
            return ""

        if not self.SERIAL_PORT.isOpen():
            return ""

        xorsum = int("aa", 16) ^ self.MID ^ int("a9", 16) ^ int("ab", 16)
        cmd_int = [ int("aa", 16), self.MID, int("a9", 16), int("ab", 16), xorsum ]
        cmd = bytearray(cmd_int)

        self.SERIAL_PORT.write(cmd)
        time.sleep(CMD_DELAY)
        result = self.SERIAL_PORT.read_all()

        if len(result) <= 0:
            return ""

        if len(result) != 10:
            return ""

        if result[0] != 0xaa:
            return ""

        if result[9] != 0xff:
            return ""

        if result[2] != self.MID:
            return ""

        if result[1] != 0xaa:
            return ""

        sum1 = result[1] + result[2] + result[3] + result[4] + result[5] + result[6]
        sum2 = (result[7] << 8) + result[8]
        if sum1 != sum2:
            return ""

        posneg = 1
        if result[3] == 0x01:
            posneg = -1
        d = posneg * ((result[4] << 16) | (result[5] << 8) | result[6])

        return d

    # E1 MID E2 E3 E4
    def SetMID(self, mid):
        if self.IS_OPEN is False:
            return False

        if not self.SERIAL_PORT.isOpen():
            return False

        cmd_int = [ int("e1", 16), int(mid, 16), int("e2", 16), int("e3", 16), int("e4", 16) ]
        cmd = bytearray(cmd_int)

        self.SERIAL_PORT.write(cmd)
        time.sleep(CMD_DELAY)
        result = self.SERIAL_PORT.read_all()

        if len(result) <= 0:
            return False

        if result[2] != mid:
            return False

        self.MID = mid

        return True

    def GetVoltage(self):
        l = self.GetSerialResult("b0")
        if len(l) <= 0:
            return ""

        result, result_hex, result_int = l
        return str("{}.{}".format(result_int[2], result_int[3]))
