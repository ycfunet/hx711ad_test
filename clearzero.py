import sys

from hx711ad_reader import Hx711adReader

if __name__ == '__main__':

    hx711 = Hx711adReader()
    hx711.Open("/dev/ttyUSB0")

    weight = hx711.ClearZero()
    print(weight)

    hx711.Close()
    sys.exit(0)
